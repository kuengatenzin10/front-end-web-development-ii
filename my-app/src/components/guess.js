import React from "react";
class GuessingGameClass extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        score: 0,  //the components state is initialized with two properties : "score" and "guess"
        guess: ""  // the properties "score" is set to 0 and "guess" is set as an empty string 
      };
      this.checkNumber = this.checkNumber.bind(this);  //The checkNumber function is bound to the component's instance using bind
    }
   
    checkNumber = (e) => {
      const randomNumber = Math.floor(Math.random() * 2) + 1;
   
      if (Number(this.state.guess) === randomNumber) {
        this.setState({ score: this.state.score + 1 });
      }
    };
   
    render() {
      return (
        <div>
          What number (between 1 and 10) am I thinking of?
          <input
            type="number"
            name="guess"
            min="1"
            max="10"
            onChange={(e) => {
              this.setState({ guess: e.target.value });
            }}
          />
          <button onClick={this.checkNumber}>Guess!</button>
          <p>Your score: {this.state.score}</p>
        </div>
      );
    }
  }
   
  export default GuessingGameClass;