import React, {useState} from "react";

export default function LoginForm(){

    const [userDate, setUserDate] = useState({email: '', password: ''});

    function emailEnteredHandler(event) {
        setUserDate({email: event.target.value,
                        password: userDate.password});
    };

    function passwordEnteredHandler(event) {
        setUserDate({email: event.target.value,
            password: userDate.password});
    };

    return (
        <form>
            <input
            type = "email" placeholder="Your email"
            onBlur={emailEnteredHandler} />

            <input
            type = "password" placeholder="Your password"
            onBlur = {passwordEnteredHandler}
            />
        </form>
    );
};
