import { useState } from "react";

const LoginForm = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [errors, setErrors] = useState({});

    const handleSubmit = (event) => {
        event.preventDefault();
    }

    const handleInputChange = (event) => {
        const {name, value} = event.target;
        if (name === 'email') {
            setEmail(value);
        } else {
            setPassword(value);
        }
    }

    const handleBlur = (event) => {
        const {name, value} = event.target;
        let error = {};
        if (!value) {
            error[name] = 'This field is required';
        } else if (name === 'email' && '/\S+@\S+\.\S+/'.test(value)) {
            error[name] = 'Please enter a valid email address';
        } else if (name === 'passsword' && value.length < 6){
            error[name] = 'Password must be atleast 6 characters long';
        } else {
            error = {};
        }
        setErrors(error);
    }

    return (
        <form onSubmit={handleSubmit}>
            <div>
                <label htmlFor="email" style ={{ color: setErrors.email ? 'red' :'black' }}>Email:</label> 
                <input type="email" name="email" value={email} onChange={handleInputChange} onBlur={handleBlur} style={{ borderColor: errors.email ? 'red' : 'initial', backgroundColor: errors.email ? '#ffe0e0' : 'initial' }} />
        {errors.email && <span style={{ color: 'red' }}>{errors.email}</span>}
      </div>
      <div>
        <label htmlFor="password" style={{ color: errors.password ? 'red' : 'black' }}>Password:</label>
        <input type="password" name="password" value={password} onChange={handleInputChange} onBlur={handleBlur} style={{ borderColor: errors.password ? 'red' : 'initial', backgroundColor: errors.password ? '#ffe0e0' : 'initial' }} />
        {errors.password && <span style={{ color: 'red' }}>{errors.password}</span>}
      </div>
      <button type="submit">Submit</button>
        </form>
    );

};
  
export default LoginForm;


