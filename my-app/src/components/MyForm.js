import React, { useRef } from 'react';
import './MyForm.css';
function MyForm() {
  const inputRef = useRef(null);
  const selectRef = useRef(null);

  function handleSubmit(event) {
    event.preventDefault();
    const inputValue = inputRef.current.value;
    const selectValue = selectRef.current.value;
    console.log(`Input value: ${inputValue}, Select value: ${selectValue}`);
  }

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Your Name: <br></br>
        <input type="text" ref={inputRef} />
      </label>
      <br />
      <label>
        Choose your Program:  <br></br>
        <select ref={selectRef}> 
          <option value="option1">Bachelor of Science Computer Science</option>
          <option value="option2">Bachelor of Science in Information Technology</option>
          <option value="option3">Option 3</option>
        </select>
      </label>
      <br />
      <button type="submit">Submit</button>
    </form>
  );
}

export default MyForm;