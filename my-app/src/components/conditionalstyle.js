import { useState } from "react";
//import styled from "styled-components";

export default function ConditionalStyle(){
    const [open, setOpen] = useState(false);
    const styles = {
        popup: {
            display: open ? "flex" : "none",
            opacity: open ? "1" : "0"
        }
    };

    return (
        <div>
            <button
            className="open_button"  
            onClick={() => {
                setOpen(true);
            }} 
            >
            Open!
            </button>

            <div className="popup" style={styles.popup}>
                <hi>This is a popup!</hi>
                <button
                className="close_button" 
                onClick={() => {
                    setOpen(false);
                }}
                >
                Close!
                </button>
            </div>

        </div>
    );
}