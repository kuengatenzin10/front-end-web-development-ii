import React, {useState} from "react";

export default function calculate() {


function Add() {
    const [num1, setNum1] = useState(0);
    const [num2, setNum2] = useState(0);
    const [result, setResult] = useState(0);
  
    const handleNum1Change = (event) => {
      setNum1(Number(event.target.value));
    };
  
    const handleNum2Change = (event) => {
      setNum2(Number(event.target.value));
    };
  
    const handleCalculate = () => {
      setResult(num1 + num2);
    };
  
    return (
      <div>
      <label>
        Num1:
        <input type="number" value={num1} onChange={handleNum1Change} />
      </label>
      <label>
        Num2:
        <input type="number" value={num2} onChange={handleNum2Change} />
      </label>
      <button onClick={handleCalculate}>Add</button>
      <p>Result: {result}</p>
    </div>
  );

  function Subtract() {
    const [num1, setNum1] = useState(0);
    const [num2, setNum2] = useState(0);
    const [result, setResult] = useState(0);
  
    const handleNum1Change = (event) => {
      setNum1(Number(event.target.value));
    };
  
    const handleNum2Change = (event) => {
      setNum2(Number(event.target.value));
    };
  
    const handleCalculate = () => {
      setResult(num1 - num2);
    };
  
    return (
      <div>
        <label>
          Num1:
          <input type="number" value={num1} onChange={handleNum1Change} />
        </label>
        <label>
          Num2:
          <input type="number" value={num2} onChange={handleNum2Change} />
        </label>
        <button onClick={handleCalculate}>Subtract</button>
        <p>Result: {result}</p>
      </div>
    );
  }
  
  function Multiply() {
    const [num1, setNum1] = useState(0);
    const [num2, setNum2] = useState(0);
    const [result, setResult] = useState(0);
  
    const handleNum1Change = (event) => {
      setNum1(Number(event.target.value));
    };
  
    const handleNum2Change = (event) => {
      setNum2(Number(event.target.value));
    };
  
    const handleCalculate = () => {
      setResult(num1 * num2);
    };
  
    return (
      <div>
        <label>
          Num1:
          <input type="number" value={num1} onChange={handleNum1Change} />
        </label>
        <label>
          Num2:
          <input type="number" value={num2} onChange={handleNum2Change} />
        </label>
        <button onClick={handleCalculate}>Multiply</button>
        <p>Result: {result}</p>
      </div>
    );
  }
  
};
};

