import React, { Component } from "react";
class EventBind extends Component {
    constructor(props) {
        super(props);
        this.state = {
            count : 0
        }
    }

    increaseCount() {
        this.setState(prevState => ({count: prevState.count + 1}));
    }
    render() {
        return(
            <div className="EventBind">
                <button onClick={this.increaseCount.bind(this)}>
                    Count:{this.state.count}
                </button>
            </div>
        )
    }
}
export default EventBind;