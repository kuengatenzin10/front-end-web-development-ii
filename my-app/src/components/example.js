export const sumOfnumbers = (num1,num2) => {
    if(num1 < 0 ||  num2 < 0 ){
      throw new Error ("one of the number is negative");
    }
    return num1+num2;
  }